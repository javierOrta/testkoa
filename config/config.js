
var host = process.env.DB_HOST || 'localhost'
var port = process.env.DB_PORT || '27017'
var dbName = 'koaSwagg'

module.exports = {
  'secret': 'algoMuySeguro',
  'dbURL': `mongodb://${host}:${port}/${dbName}`,
  
}