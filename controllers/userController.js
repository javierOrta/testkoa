const User = require('../models/User');


module.exports = {
    async index(ctx) {
      const users = await User.find();
      ctx.body = {
        status: 'success',
        data: users
      };
    },
  
    async store(ctx) {
      const { body } = ctx.request;
      let user = new User(body);
      user = await user.save();
      ctx.body = {
        status: 'success',
        data: user
      };
    },
  
    async show(ctx) {
      const { id } = ctx.params;
      const user = await User.findById(id);
      ctx.body = {
        status: 'success',
        data: user
      };
    },
   
    async update(ctx) {
      const { id } = ctx.params;
      const { body } = ctx.request;
      await user.findByIdAndUpdate(id, body);
      const user = await User.findById(id);
      ctx.body = {
        status: 'success',
        message: 'user successfully updated',
        data: user
      };
    },
  
    async destroy(ctx) {
      const { id } = ctx.params;
      await User.findByIdAndDelete(id);
      ctx.body = {
        status: 'success',
        message: 'user successfully deleted'
      };
    }
  };