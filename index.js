
const Koa = require('koa');
const router = require('./middleware/router');
const logger = require('koa-logger');
const bodyParser = require('koa-body');
const validator = require('./middleware/validator');


const app = new Koa();
const port = process.env.PORT || 3000;


// validator
app.use(validator());

//logger
app.use(logger());

//body parse
app.use(bodyParser());

//router
app.use(router.routes());
app.use(router.allowedMethods());


app.listen(port, () =>
    console.log(`Server running on http://localhost:${port}`)
);
