const KoaRouter = require('koa-router');
const userController = require('../controllers/userController');

const router = new KoaRouter();


router
  .get('/user', userController.index)
  .post('/user', userController.store)
  .get('/user/:id', userController.show)
  .put('/user/:id', userController.update)
  .delete('/user/:id', userController.destroy);


module.exports = router;