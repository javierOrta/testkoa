var mongoose = require('mongoose');
var config = require('../config/config')
//load database


mongoose.connect(
    config.dbURL,
  { useNewUrlParser: true }
);

const db = mongoose.connection;

db.on('error', error => {
  throw new Error(`error connecting to db: ${error}`);
});

db.once('open', () => console.log('database connected'));


exports.db = mongoose;